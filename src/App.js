/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import Navbar from './components/navbar';
import GetFilms from './components/films.get';
import CreateFilm from './components/film.create';
import GetFilm from './components/film.get';
import UploadFilms from './components/films.upload';
import SearchFilms from './components/films.search';

function App() {
  return (
    <Router>
      <div className="container">
        <Navbar />
        <br />
        <Route path="/" exact component={GetFilms} />
        <Route path="/get/:id" component={GetFilm} />
        <Route path="/create" component={CreateFilm} />
        <Route path="/upload" component={UploadFilms} />
        <Route path="/search" component={SearchFilms} />
      </div>
    </Router>
  );
}

export default App;
