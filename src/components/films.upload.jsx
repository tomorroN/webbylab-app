/* eslint-disable no-undef */
/* eslint-disable no-continue */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-props-no-spreading */
import axios from 'axios';
import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';

function App() {
  const onDrop = useCallback((acceptedFiles) => {
    const reader = new FileReader();

    reader.onabort = () => console.log('file reading was aborted');
    reader.onerror = () => console.log('file reading has failed');
    reader.onload = () => {
      // Do whatever you want with the file contents
      const films = [];

      const dataString = reader.result;

      const dataArray = dataString.split('\n');

      for (let i = 0; i < dataArray.length; i += 5) {
        if (!dataArray[i + 5]) continue;

        const obj = {
          title: dataArray[i].split(': ')[1],
          releaseYear: dataArray[i + 1].split(': ')[1],
          format: dataArray[i + 2].split(': ')[1],
          stars: dataArray[i + 3].split(': ')[1]
        };

        films.push(obj);
      }

      axios.post('http://localhost:5000/v1/films', films)
        .then((res) => console.log(res.data))
        .catch((error) => console.log(error));
    };

    acceptedFiles.forEach((file) => reader.readAsText(file));
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      <p>Drag 'n' drop some files here, or click to select files</p>
    </div>
  );
}

export default App;
