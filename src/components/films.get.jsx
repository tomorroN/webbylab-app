/* eslint-disable no-console */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Film = (props) => (
  <tr>
    <td>{props.film.title}</td>
    <td>{props.film.releaseYear}</td>
    <td>{props.film.format}</td>
    <td>{props.film.stars.join(', ')}</td>
    <td>
      <Link to={`/get/${props.film._id}`}>Info</Link>
      {' '}
|
      {' '}
      <a href="#" onClick={() => { props.removeFilm(props.film._id); }}>Remove</a>
    </td>
  </tr>
);

export default class GetFilms extends Component {
  constructor(props) {
    super(props);

    this.removeFilm = this.removeFilm.bind(this);

    this.state = { films: [] };
  }

  componentDidMount() {
    axios.get('http://localhost:5000/v1/films?sortBy=title')
      .then((response) => {
        this.setState({ films: response.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  removeFilm(id) {
    axios.delete(`http://localhost:5000/v1/films/${id}`)
      .then((response) => { console.log(response.data); });

    this.setState({
      films: this.state.films.filter((el) => el._id !== id)
    });
  }


  filmsList() {
    return this.state.films.length
      ? this.state.films.map((currentFilm) => (
        <Film
          film={currentFilm}
          removeFilm={this.removeFilm}
          key={currentFilm._id}
        />
      ))
      : 'Add some films first';
  }

  render() {
    return (
      <div>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Title</th>
              <th>Release year</th>
              <th>Format</th>
              <th>Actors</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            { this.filmsList() }
          </tbody>
        </table>
      </div>
    );
  }
}
