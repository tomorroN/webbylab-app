/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {
  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Films</Link>
        <div className="collpase navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="navbar-item">
              <Link to="/create" className="nav-link">Add film</Link>
            </li>
            <li className="navbar-item">
              <Link to="/upload" className="nav-link">Upload film</Link>
            </li>
            <li className="navbar-item">
              <Link to="/search" className="nav-link">Search films</Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
