/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import axios from 'axios';

export default class GetFilm extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    axios.get(`http://localhost:5000/v1/films/${this.props.match.params.id}`)
      .then((response) => {
        this.setState({ ...response.data });
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Title: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.title}
            />
          </div>
          <div className="form-group">
            <label>Release year: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.releaseYear}
            />
          </div>
          <div className="form-group">
            <label>Stars: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.stars}
            />
          </div>
          <div className="form-group">
            <label>Format: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.format}
            />
          </div>
        </form>
      </div>
    );
  }
}
