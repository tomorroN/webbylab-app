/* eslint-disable no-console */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-undef */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/prefer-stateless-function */
import axios from 'axios';
import React, { Component } from 'react';

export default class CreateFilm extends Component {
  constructor(props) {
    super(props);

    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeReleaseYear = this.onChangeReleaseYear.bind(this);
    this.onChangeFormat = this.onChangeFormat.bind(this);
    this.onAddStar = this.onAddStar.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      title: '',
      releaseYear: '',
      format: 'VHS',
      stars: []
    };
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    });
  }

  onChangeReleaseYear(e) {
    this.setState({
      releaseYear: e.target.value
    });
  }

  onChangeFormat(e) {
    this.setState({
      format: e.target.value
    });
  }

  onAddStar(e) {
    console.log(e.target.value.split(','));
    this.setState({
      stars: e.target.value.split(',')
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const film = [{ ...this.state }];

    axios.post('http://localhost:5000/v1/films', film)
      .then((res) => console.log(res.data))
      .catch((error) => console.log(error));

    window.location = '/';
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Title: </label>
            <input
              type="text"
              required
              className="form-control"
              value={this.state.title}
              onChange={this.onChangeTitle}
            />
          </div>
          <div className="form-group">
            <label>Release year: </label>
            <input
              type="number"
              required
              min={1850}
              max={2020}
              className="form-control"
              value={this.state.releaseYear}
              onChange={this.onChangeReleaseYear}
            />
          </div>
          <div className="form-group">
            <label>Stars: (Use only ',' as separator)</label>
            <input
              type="text"
              required
              pattern="^[a-zA-Z][a-zA-Z\s,]*$"
              title="Numbers is not allows"
              className="form-control"
              value={this.state.stars}
              onChange={this.onAddStar}
            />
          </div>
          <div className="form-group">
            <label>Format: </label>
            <select value={this.state.format} onChange={this.onChangeFormat}>
              <option value="VHS">VHS</option>
              <option value="DVD">DVD</option>
              <option value="Blu-Ray">Blu-Ray</option>
            </select>
          </div>

          <div className="form-group">
            <input type="submit" value="Add new film" className="btn btn-primary" />
          </div>
        </form>
      </div>
    );
  }
}
