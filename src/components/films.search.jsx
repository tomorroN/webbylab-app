/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable no-console */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-undef */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/prefer-stateless-function */

import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

const Film = (props) => (
  <tr>
    <td>{props.film.title}</td>
    <td>{props.film.releaseYear}</td>
    <td>{props.film.format}</td>
    <td>{props.film.stars.join(', ')}</td>
    <td>
      <Link to={`/get/${props.film._id}`}>Info</Link>
      {' '}
|
      {' '}
      <a href="#" onClick={() => { props.removeFilm(props.film._id); }}>Remove</a>
    </td>
  </tr>
);

export default class SearchFilm extends Component {
  constructor(props) {
    super(props);

    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeStar = this.onChangeStar.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.removeFilm = this.removeFilm.bind(this);

    this.state = {
      films: [],
      title: undefined,
      stars: undefined
    };
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    });
  }

  onChangeStar(e) {
    this.setState({
      stars: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const params = { ...this.state };

    axios.get('http://localhost:5000/v1/films', { params })
      .then((res) => {
        const { error } = res.data;

        if (error && error.message) throw new Error(error.message);
        this.setState({ films: res.data });
      })
      .catch((error) => console.log(error));
  }

  removeFilm(id) {
    axios.delete(`http://localhost:5000/v1/films/${id}`)
      .then((response) => { console.log(response.data); })
      .catch((error) => console.log(error));

    this.setState({
      films: this.state.films.filter((el) => el._id !== id)
    });
  }

  filmsList() {
    return this.state.films.length
      ? this.state.films.map((currentFilm) => (
        <Film
          film={currentFilm}
          removeFilm={this.removeFilm}
          key={currentFilm._id}
        />
      ))
      : 'Enter star or title';
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Title: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.title}
              onChange={this.onChangeTitle}
            />
          </div>
          <div className="form-group">
            <label>Star: </label>
            <input
              type="text"
              className="form-control"
              value={this.state.star}
              onChange={this.onChangeStar}
            />
          </div>

          <div className="form-group">
            <input type="submit" value="Search" className="btn btn-primary" />
          </div>
        </form>
        <div>
          <table className="table">
            <thead className="thead-light">
              <tr>
                <th>Title</th>
                <th>Release year</th>
                <th>Format</th>
                <th>Actors</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              { this.filmsList() }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
