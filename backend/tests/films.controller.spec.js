const { assert } = require('chai');
const mongoose = require('mongoose');

const { dbConfigs } = require('../src/configs');
const { DBConnection } = require('../src/database');
const { FilmsController } = require('../src/controllers');
const { FilmsModel } = require('../src/database/models');

const dbConnection = new DBConnection(mongoose);

/* eslint-disable no-undef */
describe('FilmsController', () => {
  let filmsController;
  let films;

  before(async () => {
    await dbConnection.create(dbConfigs);

    films = new FilmsModel(mongoose).init();

    filmsController = new FilmsController(films);
  });

  after(() => {
    setTimeout(process.exit, 1000);
  });

  describe('Create film', () => {
    let filmsArray = [];

    beforeEach(() => {
      filmsArray = [
        {
          title: 'Blazing Saddles',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        }
      ];
    });

    afterEach(async () => films.deleteMany({}));

    it('Should throw error (empty title)', () => {
      filmsArray[0].title = undefined;

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: title: Path `title` is required.'));
    });

    it('Should throw error (empty releaseYear)', () => {
      filmsArray[0].releaseYear = undefined;

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: releaseYear: Path `releaseYear` is required.'));
    });

    it('Should throw error (empty format)', () => {
      filmsArray[0].format = undefined;

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: format: Path `format` is required.'));
    });

    it('Should throw error (invalid format)', () => {
      filmsArray[0].format = 'foo';

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: format: `foo` is not a valid enum value for path `format`.'));
    });

    it('Should throw error (invalid stars)', () => {
      filmsArray[0].stars = [];

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: stars: Path `stars` is required.'));
    });

    it('Should throw error (empty stars)', () => {
      filmsArray[0].stars = undefined;

      filmsController.create(filmsArray)
        .catch((error) => assert.equal(error.message, 'Films validation failed: stars: Path `stars` is required.'));
    });

    it('Should create film and return type of result = object', async () => {
      assert.typeOf(await filmsController.create(filmsArray), 'object');
    });

    it('Should create film and return result that has all deep keys', async () => {
      const expectedKeys = [
        '__v',
        '_id',
        'title',
        'releaseYear',
        'format',
        'stars',
        'updatedAt',
        'createdAt'
      ];

      assert.hasAllDeepKeys(await filmsController.create(filmsArray), expectedKeys);
    });

    it('Should create films and return type of result = array', async () => {
      filmsArray.push({
        title: 'Foo',
        releaseYear: 1974,
        format: 'VHS',
        stars: [
          'Mel Brooks',
          'Clevon Little',
          'Harvey Korman',
          'Gene Wilder',
          'Slim Pickens',
          'Madeline Kahn'
        ]
      });

      assert.typeOf(await filmsController.create(filmsArray), 'array');
    });

    it('Should create films and return length of result = 2', async () => {
      filmsArray.push({
        title: 'Foo',
        releaseYear: 1974,
        format: 'VHS',
        stars: [
          'Mel Brooks',
          'Clevon Little',
          'Harvey Korman',
          'Gene Wilder',
          'Slim Pickens',
          'Madeline Kahn'
        ]
      });

      assert.lengthOf(await filmsController.create(filmsArray), 2);
    });
  });

  describe('Delete film', () => {
    let filmId = '';

    beforeEach(async () => {
      const filmsArray = [
        {
          title: 'Blazing Saddles',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        }
      ];

      const result = await films.create(...filmsArray);

      filmId = result._id;
    });

    afterEach(async () => films.deleteMany({}));

    it('Should throw error = Not found', () => {
      filmsController.remove('foo')
        .catch((error) => assert.equal(error.message, 'Not found'));
    });

    it('Should remove film and return type of result = object', async () => {
      assert.typeOf(await filmsController.remove(filmId), 'object');
    });

    it('Should create film and return result that has all deep keys', async () => {
      assert.hasAllDeepKeys(await filmsController.remove(filmId), ['message']);
    });

    it('Should remove film and return result = Film has been removed', async () => {
      const result = await filmsController.remove(filmId);

      assert.equal(result.message, 'Film has been removed');
    });
  });

  describe('GetOne film', () => {
    let filmId = '';

    beforeEach(async () => {
      const filmsArray = [
        {
          title: 'Blazing Saddles',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        }
      ];

      const result = await films.create(...filmsArray);

      filmId = result._id;
    });

    afterEach(async () => films.deleteMany({}));

    it('Should throw error = Not found', () => {
      filmsController.getOne('foo')
        .catch((error) => assert.equal(error.message, 'Not found'));
    });

    it('Should get film and return type of result = object', async () => {
      assert.typeOf(await filmsController.getOne(filmId), 'object');
    });

    it('Should get film and return result that has all deep keys', async () => {
      const expectedKeys = [
        '__v',
        '_id',
        'title',
        'releaseYear',
        'format',
        'stars',
        'updatedAt',
        'createdAt'
      ];

      assert.hasAllDeepKeys(await filmsController.getOne(filmId), expectedKeys);
    });
  });

  describe('GetMany films', () => {
    beforeEach(async () => {
      const filmsArray = [
        {
          title: 'g',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        },
        {
          title: 'a',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        },
        {
          title: 'c',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        },
        {
          title: 'b',
          releaseYear: 1974,
          format: 'VHS',
          stars: [
            'Foo',
            'Mel Brooks',
            'Clevon Little',
            'Harvey Korman',
            'Gene Wilder',
            'Slim Pickens',
            'Madeline Kahn'
          ]
        }
      ];

      await films.create(...filmsArray);
    });

    afterEach(async () => films.deleteMany({}));

    it('Should throw error = Not found', async () => {
      try {
        await films.deleteMany({});
        await filmsController.getMany();
      } catch (error) {
        assert.equal(error.message, 'Not found');
      }
    });

    it('Should get films and return type of result = array', async () => {
      assert.typeOf(await filmsController.getMany(), 'array');
    });

    it('Should get films and return length of result = 4', async () => {
      assert.lengthOf(await filmsController.getMany(), 4);
    });

    it('Should get sorted by title films (first = a)', async () => {
      const result = await filmsController.getMany(undefined, 'title');

      assert.equal(result[0].title, 'a');
    });

    it('Should get sorted by title films (second = b)', async () => {
      const result = await filmsController.getMany(undefined, 'title');

      assert.equal(result[1].title, 'b');
    });

    it('Should get sorted by title films (third = c)', async () => {
      const result = await filmsController.getMany(undefined, 'title');

      assert.equal(result[2].title, 'c');
    });

    it('Should get sorted by title films (fourth = g)', async () => {
      const result = await filmsController.getMany(undefined, 'title');

      assert.equal(result[3].title, 'g');
    });

    it('Should get films by actor name = Foo', async () => {
      const result = await filmsController.getMany({ stars: 'Foo' });

      assert.include(result[0].stars, 'Foo');
    });

    it('Should get films by film title = c', async () => {
      const result = await filmsController.getMany({ title: 'c' });

      assert.equal(result[0].title, 'c');
    });
  });
});
