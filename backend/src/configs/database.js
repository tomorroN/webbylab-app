module.exports = {
  user: process.env.DB_USER || '',
  pass: process.env.DB_PASS || '',
  host: process.env.DB_HOST || 'localhost:27017',
  name: process.env.DB_NAME || 'films'
};
