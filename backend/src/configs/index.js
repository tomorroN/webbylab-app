const app = require('./app.js');
const database = require('./database.js');

module.exports = {
  appConfigs: app,
  dbConfigs: database
};
