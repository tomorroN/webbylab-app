/* eslint-disable max-len */
/* eslint-disable no-console */
const {
  format, transports, loggers, config
} = require('winston');
const moment = require('moment');


const colors = {
  emerg: 'inverse underline magenta',
  alert: 'underline magenta',
  crit: 'inverse underline red', // Any error that is forcing a shutdown of the service or application to prevent data loss.
  error: 'underline red', // Any error which is fatal to the operation, but not the service or application
  warning: 'underline yellow', // Anything that can potentially cause application oddities
  notice: 'underline cyan', // Normal but significant condition
  info: 'underline green', // Generally useful information to log
  debug: 'underline gray'
};

const localConfigs = {
  levels: config.syslog.levels,
  level: 'debug',
  format: format.combine(
    format.colorize({ all: true, colors }),
    format((info) => Object.assign(info, { timestamp: moment().format('HH:mm:ss') }))(),
    format.printf((info) => {
      const meta = { ...info };

      delete meta.timestamp;
      delete meta.message;
      delete meta.level;

      return `[${info.timestamp}] ${info.message}${Object.keys(meta).length ? `\n${JSON.stringify(meta, null, 4)}` : ''}`;
    })
  )
};

const logger = loggers.add('logger', { levels: config.syslog.levels });

const setTransport = () => {
  logger.clear();
  logger.add(new transports.Console(localConfigs));
};

setTransport();

process.on('unhandledRejection', (error) => {
  console.error('[Process Handler] --- Unhandled Rejection');
  console.error(error);

  setTransport();

  logger.warning('[Process Handler] --- Unhandled Rejection', { error });
});

process.on('uncaughtException', (error) => {
  console.error('[Process Handler] --- Uncaught Exception');
  console.error(error);

  setTransport();

  logger.error('[Process Handler] --- Uncaught Exception', { error, stack: error.stack });

  process.exit(1);
});
