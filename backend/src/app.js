const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');

const { FilmsModel } = require('./database/models');
const { FilmsRouter } = require('./routes');
const { FilmsController } = require('./controllers');

class App {
  constructor({ port }, { logger, mongoose }) {
    this.port = port;
    this.logger = logger;
    this.mongoose = mongoose;
  }

  /**
   * Start application
   */
  start() {
    const app = express();
    const bundle = { express, logger: this.logger };

    app.use(cors());
    app.use(helmet());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use((req, res, next) => {
      const {
        path, method, body, query
      } = req;

      this.logger.info(`[${method}: ${path}] Request`, {
        body, query, labels: { source: 'request' }
      });

      next();
    });

    app.use('/v1/films', new FilmsRouter(new FilmsController(new FilmsModel(this.mongoose).init()), bundle)
      .addListener());

    app.use((error, req, res, next) => {
      this.logger.error('[Request Handler] --- Uncaught Exception', {
        error, stack: error.stack, labels: { source: 'request' }
      });

      res.status(502).send({ error });

      next();
    });

    app.listen(this.port, () => {
      this.logger.info(`Process ${process.pid} is listening on port ${this.port}`, { labels: { source: 'app' } });
    });
  }
}

module.exports = App;
