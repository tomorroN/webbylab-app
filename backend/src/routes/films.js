class FilmsRouter {
  constructor(filmsController, { cache, express, logger }) {
    this.cache = cache;
    this.logger = logger;
    this.filmsRouter = express.Router();
    this.filmsController = filmsController;
  }

  /**
   * Add route listener
   */
  addListener() {
    this.filmsRouter.post('/', async (req, res) => {
      try {
        const result = await this.filmsController.create(req.body);

        this.logger.info('[POST /] Result', { result, labels: { service: 'films' } });

        return res.send(result).status(201);
      } catch (error) {
        const { message } = error;

        this.logger.error('[POST /] Error', { error, stack: error.stack, labels: { service: 'films' } });

        if (message.includes('Films validation failed')) return res.send({ error: { message } }).status(400);
        if (message.includes('duplicate key error')) {
          return res.send({ error: { message: 'Duplicated identifiers' } }).status(400);
        }

        return res.send({ error: { message } }.status(500));
      }
    });

    this.filmsRouter.get('/', async (req, res) => {
      try {
        const { query } = req;
        const { sortBy } = query;

        delete query.sortBy;

        const result = await this.filmsController.getMany(query, sortBy);

        this.logger.info('Result', { result, labels: { service: 'films' } });

        return res.send(result).status(200);
      } catch (error) {
        this.logger.error('Error', { error, stack: error.stack, labels: { service: 'films' } });

        if (error.message === 'Not found') return res.send({ error: { message: error.message } }).status(404);
        return res.send({ error: { message: error.message } }).status(500);
      }
    });

    this.filmsRouter.get('/:id', async (req, res) => {
      try {
        const result = await this.filmsController.getOne(req.params.id);

        this.logger.info('Result', { result, labels: { service: 'films' } });

        return res.send(result).status(200);
      } catch (error) {
        this.logger.error('Error', { error, stack: error.stack, labels: { service: 'films' } });

        if (error.message === 'Not found') return res.send({ error: { message: error.message } }).status(404);
        return res.send({ error: { message: error.message } }).status(500);
      }
    });

    this.filmsRouter.delete('/:id', async (req, res) => {
      try {
        const result = await this.filmsController.remove(req.params.id);

        this.logger.info('Result', { result, labels: { service: 'films' } });

        return res.send(result).status(204);
      } catch (error) {
        this.logger.error('Error', { error, stack: error.stack, labels: { service: 'films' } });

        if (error.message === 'Not found') return res.send({ error: { message: error.message } }).status(404);
        return res.send({ error: { message: error.message } }).status(500);
      }
    });

    return this.filmsRouter;
  }
}

module.exports = FilmsRouter;
