class FilmController {
  constructor(films) {
    this.films = films;
  }

  /**
   * Create new films
   * @param {array<object>} newFilms New films
   * @returns {any} Created films data (Single => object, Many => Array)
   */
  async create(newFilms) {
    const result = await this.films.create(...newFilms);

    return Array.isArray(result) ? result : result.toObject();
  }

  /**
   * Get one film
   * @param {string} id Film identifier
   * @returns {object} Film data
   */
  async getOne(id) {
    try {
      const film = await this.films.findById(id);

      return film.toObject();
    } catch (error) {
      if (error.message.includes('Cast to ObjectId failed for value')) throw new Error('Not found');
      throw error;
    }
  }

  /**
   * Get many films
   * @param {object} query Search query ({stars: 'bar', title: 'foo'})
   * @param {string} sortBy Sort by some film field
   * @returns {array} Films array
   */
  async getMany(query, sortBy) {
    const films = sortBy
      ? await this.films.find(query)
        .collation({ locale: 'en' })
        .sort({ [sortBy]: 1 })
        .lean()
      : await this.films.find(query)
        .lean();

    if (!films.length) throw new Error('Not found');
    return films;
  }

  /**
   * Remove film
   * @param {string} id Film identifier
   * @returns {object} Removed film data
   */
  async remove(id) {
    try {
      await this.films.findByIdAndRemove(id);

      return { message: 'Film has been removed' };
    } catch (error) {
      if (error.message.includes('Cast to ObjectId failed for value')) throw new Error('Not found');
      throw error;
    }
  }
}

module.exports = FilmController;
