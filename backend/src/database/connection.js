class DBConnection {
  constructor(mongoose) {
    this.mongoose = mongoose;
  }

  /**
   * Create database connection
   * @param {object} dbConfig Database connection config
   * @returns {Promise<object>} Database models
   */
  create({
    user, pass, host, name
  }) {
    if (!host || !name) throw new Error('Database options are not supplied.');

    let url;

    if (!user || !pass) url = `mongodb://${host}/${name}`;
    else url = `mongodb://${user}:${pass}@${host}/${name}`;

    return this.mongoose.connect(url, {
      useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false, useCreateIndex: true
    });
  }
}

module.exports = DBConnection;
