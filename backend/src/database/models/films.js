class FilmsModel {
  constructor(mongoose) {
    this.mongoose = mongoose;
  }

  /**
   * Films model initialization
   */
  init() {
    const schema = this.mongoose.Schema({
      title: { type: this.mongoose.Schema.Types.String, required: true, unique: true },
      releaseYear: { type: this.mongoose.Schema.Types.Number, required: true },
      format: { type: this.mongoose.Schema.Types.String, enum: ['VHS', 'DVD', 'Blu-Ray'], required: true },
      stars: { type: [this.mongoose.Schema.Types.String], required: true },

    }, { timestamps: true });

    return this.mongoose.model('Films', schema);
  }
}

module.exports = FilmsModel;
