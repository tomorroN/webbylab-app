require('./utils/logger');
require('dotenv').config();

const logger = require('winston').loggers.get('logger');
const mongoose = require('mongoose');

const { dbConfigs, appConfigs } = require('./configs');

const { DBConnection } = require('./database');
const App = require('./app');

const dbConnection = new DBConnection(mongoose);

dbConnection.create(dbConfigs)
  .then(() => {
    logger.info('Database connection is established', { labels: { source: 'database' } });

    new App(appConfigs, { logger, mongoose }).start();
  })
  .catch((error) => {
    logger.crit('Database connection error', { error, stack: error.stack, labels: { source: 'database' } });

    process.exit(1);
  });
